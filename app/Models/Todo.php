<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $guarded=[];
    const ID = 'id';
    const NAME ='name';
    
    use HasFactory;
}
